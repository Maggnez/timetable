﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Adaptor
{
	/// <summary>
	/// Abstract superclass to construct
	/// output parsers.
	/// </summary>
	public class OutputParser
	{
		public virtual void Parse(string file, string year) { }

		public void Parser(string file, string year)
		{
			Parse(file, year);
		}
	}
}