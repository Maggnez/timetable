﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using API.Models;
using API.Models.Rooms;
using System.Configuration;
using System.Collections;
using System.Windows.Forms;

namespace Adaptor
{
	public interface IDbRequestInHandler
	{
		void PostBooking(List<KeyValuePair<int, BookingViewModel>> courseOfBooking, Hashtable courseName);
	}

	/// <summary>
	/// The DbRequestInHandler takes care of sending post
	/// requests with the roombooking object to Centris API.
	/// </summary>
	public class BookRooms : IDbRequestInHandler
	{
		/// <summary>
		/// PostBooking uses BookingViewModel class from Centris
		/// API to book a room.
		/// </summary>
		/// <param name="roomId"></param>
		/// <param name="booking"></param>
		void IDbRequestInHandler.PostBooking(List<KeyValuePair<int, BookingViewModel>> courseOfBooking, Hashtable courseName)
		{
			var url = ConfigurationManager.AppSettings["CentrisURL"];
			var client = new HttpClient();
			var failedBookings = new Dictionary<BookingViewModel, String>();

			client.BaseAddress = new Uri(url);
			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

			foreach(KeyValuePair<int, BookingViewModel> item in courseOfBooking)
			{
				if (item.Key == 0)
				{
					failedBookings.Add(item.Value, " (No classroom chosen)");
					continue;
				}
				try
				{
					HttpResponseMessage response = client.PostAsJsonAsync("api/v1/rooms/" + item.Key + "/bookings", item.Value).Result;

					Console.WriteLine(response);

					if(!response.IsSuccessStatusCode)
					{
						try
						{
							failedBookings.Add(item.Value, " (Room already booked)");
						}
						catch 
						{
							continue;
						}
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					System.Windows.Forms.MessageBox.Show("Could not connect to Centris. Please make sure you are connected to Reykjavík University");
					
					return;
				} 
			}

			StringBuilder sb = new StringBuilder();

			if (failedBookings.Count() != 0)
			{
				sb.AppendLine("The following courses could not be booked:");

				foreach (KeyValuePair<BookingViewModel, String> item in failedBookings)
				{
					if (item.Key.Recurrence == null)
					{
						sb.AppendFormat("{0, -30} {1,10}: {2,10} {3,10} {4}", courseName[item.Key.CourseID.ToString()].ToString(), "   at date:   ", item.Key.Bookings[0].StartTime.ToString(), item.Value);
					}
					else
					{
						String myString = courseName[item.Key.CourseID.ToString()].ToString();
						sb.AppendFormat("{0, -30} {1,10}: {2,10} {3,10} {4}", myString, " at date ", item.Key.Recurrence.Begins.ToString(), item.Value, Environment.NewLine);
					}
				}

				FailedBookings FB = new FailedBookings();
				FB.RText = sb.ToString();
				DialogResult dialogresult = FB.ShowDialog();
			}
			else
			{
				System.Windows.Forms.MessageBox.Show("All rooms were booked successfully");
			}
		}
	}
}