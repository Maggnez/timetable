﻿using Adaptor.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Adaptor.Asc
{
	/// <summary>
	/// This is the AscInputParser, it has access to an object
	/// that the xml output from CentrisAPI has been converted into.
	/// These xml objects are used to parse the data to the form
	/// that aSc TimeTables expects.
	/// </summary>
	public class AscInputParser : InputParser
	{
		/// <summary>
		/// Reads the room object and creates a file that is ready to be used by asc
		/// </summary>
		public override void RoomParser(List<RoomObject> rooms, string folder)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.ConformanceLevel = ConformanceLevel.Document;
			XmlWriter writer = XmlWriter.Create(folder + "\\" + "AscRoomsOutput.xml", settings);
			writer.WriteStartDocument();
			writer.WriteStartElement("timetable");
			writer.WriteAttributeString("importtype", "database");
			writer.WriteAttributeString("options", "idprefix:MyApp");
			writer.WriteStartElement("classrooms");
			writer.WriteAttributeString("options", "");
			writer.WriteAttributeString("columns", "id,name,short,capacity");

			foreach (RoomObject i in rooms)
			{
				writer.WriteStartElement("classroom");

				writer.WriteAttributeString("id", i.RoomID);
				writer.WriteAttributeString("name", i.RoomName);
				writer.WriteAttributeString("short", i.RoomName);
				writer.WriteAttributeString("capacity", i.RoomSeatCount);

				writer.WriteEndElement();
			}
			writer.WriteEndElement();
			writer.WriteEndElement();
			writer.WriteEndDocument();
			
			writer.Close();
		}

		/// <summary>
		/// Reads the teacher object and creates a file that is ready to be used by asc.
		/// </summary>
		public override void TeacherParser(List<TeacherObject> teachers, string folder)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.ConformanceLevel = ConformanceLevel.Document;
			XmlWriter writer = XmlWriter.Create(folder + "\\" + "AscTeacherOutput.xml", settings);
			
			writer.WriteStartDocument();
			writer.WriteStartElement("timetable");
			writer.WriteAttributeString("importtype", "database");
			writer.WriteAttributeString("options", "idprefix:MyApp");
			writer.WriteStartElement("teachers");
			writer.WriteAttributeString("options", "idprefix:MyApp");
			writer.WriteAttributeString("columns", "id,name,short");

			foreach (TeacherObject i in teachers)
			{
				writer.WriteStartElement("teacher");

				writer.WriteAttributeString("id", i.TeacherSSN);
				writer.WriteAttributeString("name", i.TeacherName);
				writer.WriteAttributeString("short", i.TeacherName);

				writer.WriteEndElement();
			}
			writer.WriteEndElement();
			writer.WriteEndDocument();
			writer.Close();
		}

		/// <summary>
		/// Reads the course object and creates a file that is ready to be used by asc
		/// </summary>
		public override void CourseParser(List<CourseObject> courses, string folder)
		{
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.ConformanceLevel = ConformanceLevel.Document;
			XmlWriter writer = XmlWriter.Create(folder + "\\" + "AscSubjectOutput.xml", settings);
			StringBuilder sb = new StringBuilder();
			String sbString = "";
			writer.WriteStartDocument();
			writer.WriteStartElement("timetable");
			writer.WriteAttributeString("importtype", "database");
			writer.WriteAttributeString("options", "idprefix:MyApp");
			writer.WriteStartElement("subjects");
			writer.WriteAttributeString("options", "idprefix:MyApp,customfield1:StartDate,customfield2:EndDate");
			writer.WriteAttributeString("columns", "id,name,short,customfield1,customfield2");

			foreach (CourseObject i in courses)
			{
				writer.WriteStartElement("subject");

				writer.WriteAttributeString("id", i.CourseID);
				writer.WriteAttributeString("name", i.CourseName);
				writer.WriteAttributeString("short", i.CourseName);
				writer.WriteAttributeString("customfield1", i.DateStarts);
				writer.WriteAttributeString("customfield2", i.DateEnds);

				writer.WriteEndElement();
			}
			writer.WriteEndElement();

			writer.WriteStartElement("lessons");
			writer.WriteAttributeString("options", "");
			writer.WriteAttributeString("columns", "subjectid,capacity,teacherids");
			foreach (CourseObject i in courses)
			{
				writer.WriteStartElement("lesson");

				writer.WriteAttributeString("subjectid", i.CourseID);
				writer.WriteAttributeString("capacity", i.RegisteredStudentsInCourse);
				foreach(KeyValuePair<String, String> item in i.Teachers)
				{
					sb.Append(item.Key + ",");
				}
				sbString = sb.ToString();
				writer.WriteAttributeString("teacherids", sbString.Remove(sbString.Length - 1));
				sb.Clear();
				writer.WriteEndElement();
			}
			writer.WriteEndElement();
			writer.WriteEndDocument();
			writer.Close();
		}
	}
}