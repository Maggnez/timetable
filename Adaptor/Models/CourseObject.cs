﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptor.Models
{
	/// <summary>
	/// CourseObject contains all needed information of a course.
	/// </summary>
	public class CourseObject
	{
		private String courseID;
		private String courseName;
		private String registeredStudentsInCourse;
		private String dateStarts;
		private String dateEnds;
		private List<KeyValuePair<String, String>> teachers = new List<KeyValuePair<String, String>>();

		public CourseObject()
		{
			
		}

		public String CourseID
		{
			get
			{
				return courseID;
			}
			set
			{
				courseID = value;
			}
		}

		public String CourseName
		{
			get
			{
				return courseName;
			}
			set
			{
				courseName = value;
			}
		}

		public String RegisteredStudentsInCourse
		{
			get
			{
				return registeredStudentsInCourse;
			}
			set
			{
				registeredStudentsInCourse = value;
			}
		}

		public String DateStarts
		{
			get
			{
				return dateStarts;
			}
			set
			{
				dateStarts = value;
			}
		}

		public String DateEnds
		{
			get
			{
				return dateEnds;
			}
			set
			{
				dateEnds = value;
			}
		}

		public List<KeyValuePair<String, String>> Teachers
		{
			get
			{
				return teachers;
			}
			set
			{
				teachers = value;
			}
		}
	}
}
