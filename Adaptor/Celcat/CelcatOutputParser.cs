﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using API.Models;
using API.Models.Rooms;
using System.Globalization;
using System.Runtime.Serialization.Json;
using System.Configuration;

namespace Adaptor.Celcat
{
	/// <summary>
	/// This is the CelcatOutputParser, it takes care of extracting
	/// necessary data from CELCAT to book a room for a 
	/// given timetable.
	/// </summary>
	public class CelcatOutputParser : OutputParser
	{
		private readonly IDbRequestInHandler _roomBooking;

		public CelcatOutputParser()
		{
			_roomBooking = new BookRooms();
		}

		public CelcatOutputParser(IDbRequestInHandler roomBooking)
		{
			_roomBooking = roomBooking;
		}

		/// <summary>
		/// The parser gets the needed attributes from the
		/// csv file that is delivered from CELCAT
		/// and uses them to create a booking object
		/// and calls the DbRequestInHandler to
		/// book a given room in CentrisAPI.
		/// </summary>
		public override void Parse(string file, String year)
		{
			if (Path.GetExtension(file) != ".csv")
			{
				System.Windows.Forms.MessageBox.Show("Input file for Celcat must be a csv file");
				return;
			}

			System.IO.StreamReader reader = new System.IO.StreamReader(file);

			List<KeyValuePair<int, BookingViewModel>> courseOfBooking = new List<KeyValuePair<int, BookingViewModel>>();

			Hashtable roomId = new Hashtable();
			Hashtable courseId = new Hashtable();

			String line, weekDay = "", startTime = "", endTime = "", subjectId = "";
 
			int classroomId;
			int weeks = 0;

			string[] values;

			DateTime start, end;

			while ((line = reader.ReadLine()) != null)
			{
				values = line.Split(',');

				// Maps room id with room name
				if (values[0] == "CT_ROOM")
				{
					line = reader.ReadLine();

					while ((line = reader.ReadLine()) != "")
					{
						values = line.Split(',');
						roomId.Add(values[1], values[2]);
					}
				}

				//Maps course id with course name
				else if (values[0] == "CT_MODULE")
				{
					line = reader.ReadLine();

					while ((line = reader.ReadLine()) != "")
					{
						values = line.Split(',');
						courseId.Add(values[1], values[2]);
					}
				}

				//Creates the booking objects and sends them
				else if(values[0] == "CT_EVENT")
				{
					line = reader.ReadLine();

					while ((line = reader.ReadLine()) != "" && line != null)
					{
						values = line.Split(',');
						weekDay = values[1];

						startTime = values[2];
						endTime = values[3];

						var getRepeat = values[4].Split('-');

						string result1 = Regex.Replace(getRepeat[0], @"[^\d]", "");
						string result2 = Regex.Replace(getRepeat[1], @"[^\d]", "");

						weeks = ((Convert.ToInt16(result2) - Convert.ToInt16(result1)) + 1);
						line = reader.ReadLine();
						values = line.Split(',');
						subjectId = values[22].ToString();
						line = reader.ReadLine();
						values = line.Split(',');

						classroomId = Convert.ToInt16(values[22]);
						var roomName = (roomId[values[22]]).ToString();

						start = DateTime.Parse(year + "-01-01T" + startTime);
						end = DateTime.Parse(year + "-01-01T" + endTime);

						start.AddDays((Convert.ToInt16(result1) - 1) * 7);

						if (weeks < 2)
						{
							BookingViewModel booking = new BookingViewModel
							{
								CourseID = Convert.ToInt16(subjectId),
								Bookings = new List<BookingDTO>
								{
									new BookingDTO 
									{
										StartTime = start,
										EndTime   = end,
										RoomName  = roomName
									}
								}
							};

							courseOfBooking.Add(new KeyValuePair<int, BookingViewModel>(classroomId, booking));
						}
						else
						{
							BookingViewModel booking = new BookingViewModel
							{
								CourseID = System.Convert.ToInt16(subjectId),
								Recurrence = new RecurrenceViewModel
								{
									Type = 2,
									Occurs = weeks,
									Begins = start,
									Ends = end
								}
							};

							switch (weekDay)
							{
								case "Mon":
									booking.Recurrence.WeekMonday = true;
									break;
								case "Tue":
									booking.Recurrence.WeekTuesday = true;
									break;
								case "Wen":
									booking.Recurrence.WeekWednesday = true;
									break;
								case "Thu":
									booking.Recurrence.WeekThursday = true;
									break;
								case "Fri":
									booking.Recurrence.WeekFriday = true;
									break;
								case "Sat":
									booking.Recurrence.WeekSaturday = true;
									break;
								case "Sun":
									booking.Recurrence.WeekSunday = true;
									break;
							}

							courseOfBooking.Add(new KeyValuePair<int, BookingViewModel>(classroomId, booking));
						}
					}
				}
			}
			_roomBooking.PostBooking(courseOfBooking, courseId);
		}
	}
}
