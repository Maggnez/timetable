﻿using Adaptor.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Adaptor.Celcat
{
	/// <summary>
	/// This is the CelcatInputParser, it has access to an object
	/// that the xml output from CentrisAPI has been converted into.
	/// These xml objects are used to parse the data to the form
	/// that CELCAT expects.
	/// </summary>
	public class CelcatInputParser : InputParser
	{
		/// <summary>
		/// Reads the room object and creates a file that is ready to be used by Celcat
		/// </summary>
		public override void RoomParser(List<RoomObject> rooms, string folder)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("CT_ROOM");
			sb.AppendLine("_room_id,unique_name,_name,_default_capacity");

			foreach (RoomObject i in rooms)
			{
				sb.Append(i.RoomID + ",");			// Room id
				sb.Append(i.RoomID + ",");			// Unique name - ID is used for that
				sb.Append(i.RoomName + ",");
				sb.AppendLine(i.RoomSeatCount);
			}

			StreamWriter outfile = new StreamWriter(folder + "\\" + "CelcatRoomsOutput.csv");
			outfile.Write(sb.ToString());
			outfile.Flush();
			outfile.Close();
		}

		/// <summary>
		/// Reads the teacher object and creates a file that is ready to be used by Celcat
		/// </summary>
		public override void TeacherParser(List<TeacherObject> teachers, string folder)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("CT_STAFF");
			sb.AppendLine("_unique_name,_staff_id,_name");

			foreach (TeacherObject i in teachers)
			{
				// 2x because unique name and id is the same.
				sb.Append(i.TeacherSSN + ",");
				sb.Append(i.TeacherSSN + ",");
				sb.AppendLine(i.TeacherName);
			}

			StreamWriter outfile = new StreamWriter(folder + "\\" + "CelcatTeacherOutput.csv");
			outfile.Write(sb.ToString());
			outfile.Flush();
			outfile.Close();
		}

		/// <summary>
		/// Reads the course object and creates a file that is ready to be used by Celcat
		/// </summary>
		public override void CourseParser(List<CourseObject> courses, string folder)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("CT_MODULE");
			sb.AppendLine("_module_id,_unique_name,_name");

			foreach (CourseObject i in courses)
			{
				sb.Append(i.CourseID + ",");

				sb.Append(i.CourseID + ",");
				sb.AppendLine(i.CourseName);
			}

			StreamWriter outfile = new StreamWriter(folder + "\\" + "CelcatCourseOutput.csv");
			outfile.Write(sb.ToString());
			outfile.Flush();
			outfile.Close();
		}
	}
}