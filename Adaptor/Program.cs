﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using System.Reflection;
using Adaptor.Celcat;
using System.Xml;
using Adaptor.Models;
using System.Windows.Forms;

namespace Adaptor
{
	/// <summary>
	/// The Program class takes care of running the application.
	/// </summary>
	public class Program
	{
		public static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new CESAR());
		}
	}
}