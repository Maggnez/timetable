﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using System.Xml;

namespace Adaptor
{
	/// <summary>
	/// The DbRequestOutHandler takes care of sending get
	/// requests to the CentrisAPI and requiring the needed
	/// information for the 3rd party softwares.
	/// </summary>
	public class RequestData
	{
		public static async Task<XmlResponse> HttpInitialize(String url, String token, String semester, String path, String system)
		{
			XmlResponse storeXml = new XmlResponse();

			var client = new HttpClient();
			client.BaseAddress = new Uri(url);

			if (!string.IsNullOrEmpty(token))
			{
				client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
			}

			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));

			String[] singleXml = await Task<String[]>.WhenAll(
				new Task<String>[] {
					RequestData.ReadRooms(client),
					RequestData.ReadCourses(client, semester),
					RequestData.ReadTeachers(client),
				}
			);

			storeXml.XmlRooms = singleXml[0];
			storeXml.XmlCourses = singleXml[1];
			storeXml.XmlTeachers = singleXml[2];

			var parser = (InputParser)Activator.CreateInstance(Type.GetType(system + "InputParser"));
			parser.Parser(storeXml, path);

			return storeXml;
		}

		/// <summary>
		/// HTTP GET all rooms
		/// </summary>
		/// <param name="client"></param>
		/// <returns></returns>
		public static async Task<String> ReadRooms(HttpClient client)
		{
			HttpResponseMessage response = await client.GetAsync("api/v1/rooms");

			if (response.IsSuccessStatusCode)
			{
				string rooms = await response.Content.ReadAsStringAsync();
				return rooms;
			}

			return "";
		}

		/// <summary>
		/// HTTP GET all teachers
		/// </summary>
		/// <param name="client"></param>
		/// <returns></returns>
		public static async Task<String> ReadTeachers(HttpClient client)
		{
			HttpResponseMessage response = await client.GetAsync("api/v1/teachers");

			if (response.IsSuccessStatusCode)
			{
				string teachers = await response.Content.ReadAsStringAsync();
				return teachers;
			}

			return "";
		}

		/// <summary>
		/// HTTP GET all courses for the current semester
		/// </summary>
		/// <param name="client"></param>
		/// <param name="semester"></param>
		/// <returns></returns>
		public static async Task<String> ReadCourses(HttpClient client, String semester)
		{
			bool includeTeachers = true;

			HttpResponseMessage response = await client.GetAsync("api/v1/courses/semester/" + semester + "?includeTeachers=" + includeTeachers);

			if (response.IsSuccessStatusCode)
			{
				string courses = await response.Content.ReadAsStringAsync();
				return courses;
			}

			return "";
		}
	}
}