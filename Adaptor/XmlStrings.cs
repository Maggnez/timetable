﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adaptor
{
	public class XmlStrings
	{
		public static String XmlRooms { get; set; }
		public static String XmlRoomOutput { get; set; }

		public static String XmlSubject { get; set; }
		public static String XmlSubjectOutput { get; set; }

		public static String XmlTeachers { get; set; }
		public static String XmlTeachersOutput { get; set; }

		public static String XmlRoomsType { get; set; }
		public static String XmlRoomsTypeOutput { get; set; }
	}
}