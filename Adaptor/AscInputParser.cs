﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Adaptor
{
    public class AscInputParser : InputParser
    {
        /// <summary>
        /// Parses the xml room output from database and picks out the information that is needed
        /// </summary>
        public override void RoomParser()
        {
            XmlReader reader = XmlReader.Create(new StringReader(XmlStrings.XmlRooms));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Document;
            XmlWriter writer = XmlWriter.Create("RoomsOutput.xml", settings);
            writer.WriteStartDocument();
            
            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "RoomDTO"))
                {
                    writer.WriteStartElement("classroom");
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "Name"))
                {
                    string name = reader.ReadInnerXml();
                    writer.WriteAttributeString("name", name);
                    writer.WriteAttributeString("short", name);
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "SeatCount"))
                {
                    writer.WriteAttributeString("capacity", reader.ReadInnerXml());
                }
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "RoomDTO"))
                {
                    writer.WriteEndElement();
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ArrayOfRoomDTO"))
                {
                    writer.WriteStartElement("timetable");
                    writer.WriteAttributeString("importtype", "database");
                    writer.WriteAttributeString("options", "idprefix:MyApp");
                    writer.WriteStartElement("classrooms");
                    writer.WriteAttributeString("options", "");
                    writer.WriteAttributeString("columns", "name,short,capacity");
                }
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "ArrayOfRoomDTO"))
                {
                    writer.WriteEndElement();
                }
            }

            writer.WriteEndElement();
            writer.Close();

            XmlStrings.XmlRoomOutput = File.ReadAllText("RoomsOutput.xml");
        }

        /// <summary>
        /// Parses the xml teachers output from database and picks out the information that is needed
        /// </summary>
        public override void TeacherParser()
        {
            XmlReader reader = XmlReader.Create(new StringReader(XmlStrings.XmlTeachers));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Document;
            XmlWriter writer = XmlWriter.Create("TeacherOutput.xml", settings);
            writer.WriteStartDocument();

            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "PersonDTO"))
                {
                    writer.WriteStartElement("teacher");
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "Name"))
                {
                    String name;
                    name = reader.ReadInnerXml();
                    writer.WriteAttributeString("name", name);
                    writer.WriteAttributeString("short", name);
                }
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "PersonDTO"))
                {
                    writer.WriteEndElement();
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ArrayOfPersonDTO"))
                {
                    writer.WriteStartElement("timetable");
                    writer.WriteAttributeString("importtype", "database");
                    writer.WriteAttributeString("options", "idprefix:MyApp");
                    writer.WriteStartElement("teachers");
                    writer.WriteAttributeString("options", "");
                    writer.WriteAttributeString("columns", "name,short");
                }
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "ArrayOfPersonDTO"))
                {
                    writer.WriteEndElement();
                }
            }

            writer.WriteEndDocument();
            writer.Close();

            XmlStrings.XmlTeachersOutput = File.ReadAllText("TeacherOutput.xml");
        }

        /// <summary>
        /// Parses the xml subjects output from database and picks out the information that is needed
        /// </summary>
        public override void SubjectParser()
        {
            XmlReader reader = XmlReader.Create(new StringReader(XmlStrings.XmlSubject));
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.ConformanceLevel = ConformanceLevel.Document;
            XmlWriter writer = XmlWriter.Create("SubjectOutput.xml", settings);
            writer.WriteStartDocument();

            while (reader.Read())
            {
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "CourseInstanceDTO"))
                {
                    writer.WriteStartElement("subject");
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "Name"))
                {
                    writer.WriteAttributeString("name", reader.ReadInnerXml());
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "CourseID"))
                {
                    writer.WriteAttributeString("short", reader.ReadInnerXml());
                }
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "CourseInstanceDTO"))
                {
                    writer.WriteEndElement();
                }
                if ((reader.NodeType == XmlNodeType.Element) && (reader.Name == "ArrayOfCourseInstanceDTO"))
                {
                    writer.WriteStartElement("timetable");
                    writer.WriteAttributeString("importtype", "database");
                    writer.WriteAttributeString("options", "idprefix:MyApp");
                    writer.WriteStartElement("subjects");
                    writer.WriteAttributeString("options", "");
                    writer.WriteAttributeString("columns", "name,short");
                }
                if ((reader.NodeType == XmlNodeType.EndElement) && (reader.Name == "ArrayOfCourseInstanceDTO"))
                {
                    writer.WriteEndElement();
                }
            }

            writer.WriteEndDocument();
            writer.Close();

            XmlStrings.XmlSubjectOutput = File.ReadAllText("SubjectOutput.xml");
        }
    }
}
