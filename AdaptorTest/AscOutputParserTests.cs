﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.IO;
using Adaptor;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Adaptor.Asc;

namespace AdaptorTest
{
	[TestClass]
	public class AscOutputParserTests
	{
		private OutputParser _parser;
		private MockDbRequestInHandler _mockDbHandler;

		[TestInitialize]
		public void Setup()
		{
			_mockDbHandler = new MockDbRequestInHandler();
			_parser = new AscOutputParser(_mockDbHandler);
		}

		[TestMethod]
		public void AscOutputParserNoRecurrenceTest()
		{
			// Arrange:
			string testXml = "<?xml version=\"1.0\" encoding=\"windows-1252\"?><timetable ascttversion=\"2015.8.2\" importtype=\"database\" options=\"idprefix:MyApp\" displayname=\"Háskólinn í Reykjavík\" displaycountries=\"is\"><periods options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"period,name,short,starttime,endtime\"><period name=\"0\" short=\"0\" period=\"0\" starttime=\"7:10\" endtime=\"7:55\"/></periods><daysdefs options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,days,name,short\"><daysdef id=\"*3\" name=\"Mánudagur\" short=\"Mán.\" days=\"1000000\"/></daysdefs><weeksdefs options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,weeks,name,short\"><weeksdef id=\"*1\" name=\"Allar vikur\" short=\"Allir\" weeks=\"1\"/></weeksdefs><termsdefs options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,terms,name,short\"><termsdef id=\"*1\" name=\"Allt árið\" short=\"YR\" terms=\"1\"/></termsdefs><subjects options=\"import:disable,canadd,canremove,canupdate,primarytt,silent,customfield1:Date\" columns=\"id,name,short,customfield1\"><subject id=\"23604\" name=\"Þjóðhagfræði\" short=\"V-103-THAG\" customfield1=\"2012-08-20T00:00:00\" customfield2=\"2012-11-30T00:00:00\"/></subjects>   <classrooms options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,name,short,classroomids,teacherid,grade\"><classroom id=\"260\" name=\"LHÍ - stofa 301\" short=\"LHÍ - stofa 301\" teacherid=\"\" classroomids=\"\" grade=\"\"/></classrooms><lessons options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,subjectid,classids,groupids,teacherids,classroomids,periodspercard,periodsperweek,daysdefid,weeksdefid,termsdefid,seminargroup,capacity\"><lesson id=\"*1\" classids=\"\" subjectid=\"23604\" periodspercard=\"3\" periodsperweek=\"6.0\" teacherids=\"336761\" classroomids=\"260\" groupids=\"\" capacity=\"*\" seminargroup=\"\" termsdefid=\"*1\" weeksdefid=\"*1\" daysdefid=\"*1\"/></lessons><cards options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"lessonid,period,days,weeks,terms,classroomid,subjectid\"><card subjectid=\"23604\" lessonid=\"*1\" classroomid=\"260\" period=\"0\" weeks=\"1\" terms=\"1\" days=\"0001000\"/></cards></timetable>";
			String year = "2012-01-01T";
			System.IO.StreamWriter write = new System.IO.StreamWriter("asc no rec test.xml");
			write.WriteLine(testXml);
			write.Flush();
			write.Close();

			// Act:
			_parser.Parse(Path.GetFullPath("asc no rec test.xml"), year);

			// Assert:
			Assert.AreEqual(1, _mockDbHandler.Bookings.Count, "The amount of booking objects with no recurrence is not correct!");
		}

		[TestMethod]
		public void AscOutputParserWithRecurrenceTest()
		{
			// Arrange:
			string testXml = "<?xml version=\"1.0\" encoding=\"windows-1252\"?><timetable ascttversion=\"2015.8.2\" importtype=\"database\" options=\"idprefix:MyApp\" displayname=\"Háskólinn í Reykjavík\" displaycountries=\"is\"><periods options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"period,name,short,starttime,endtime\"><period name=\"4\" short=\"4\" period=\"4\" starttime=\"11:00\" endtime=\"11:45\"/></periods><daysdefs options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,days,name,short\"><daysdef id=\"*6\" name=\"Fimmtudagur\" short=\"Fim.\" days=\"0001000\"/></daysdefs><weeksdefs options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,weeks,name,short\"><weeksdef id=\"*1\" name=\"Allar vikur\" short=\"Allir\" weeks=\"12\"/></weeksdefs><termsdefs options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,terms,name,short\"><termsdef id=\"*1\" name=\"Allt árið\" short=\"YR\" terms=\"1\"/></termsdefs><subjects options=\"import:disable,canadd,canremove,canupdate,primarytt,silent,customfield1:Date,customfield2:Date2\" columns=\"id,name,short,customfield1\"><subject id=\"23604\" name=\"Þjálfunarlífeðlisfræði hreyfinga\" short=\"E-712-EPHY\" customfield1=\"2012-08-20T00:00:00\" customfield2=\"2012-11-20T00:00:00\"/></subjects><teachers options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,name,short,gender,color,email,mobile\"><teacher id=\"341003\" name=\"Ingólfur Örn Þorbjörnsson\" short=\"Ingólfur Örn Þorbjörnsson\" gender=\"F\" color=\"#993333\" email=\"\" mobile=\"\"/></teachers><classrooms options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,name,short,classroomids,teacherid,grade\"><classroom id=\"177\" name=\"V114 Heilbrigðisverkfræðistofa\" short=\"V114 Heilbrigðisverkfræðistofa\" teacherid=\"\" classroomids=\"\" grade=\"\"/></classrooms><lessons options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"id,subjectid,classids,groupids,teacherids,classroomids,periodspercard,periodsperweek,daysdefid,weeksdefid,termsdefid,seminargroup,capacity\"><lesson id=\"*1\" classids=\"\" subjectid=\"23604\" periodspercard=\"3\" periodsperweek=\"6.0\" teacherids=\"336761\" classroomids=\"260\" groupids=\"\" capacity=\"*\" seminargroup=\"\" termsdefid=\"*1\" weeksdefid=\"*1\" daysdefid=\"*1\"/><lesson id=\"*2\" classids=\"\" subjectid=\"23659\" periodspercard=\"2\" periodsperweek=\"4.0\" teacherids=\"336761\" classroomids=\"177\" groupids=\"\" capacity=\"*\" seminargroup=\"\" termsdefid=\"*1\" weeksdefid=\"*1\" daysdefid=\"*1\"/></lessons><cards options=\"import:disable,canadd,canremove,canupdate,primarytt,silent\" columns=\"lessonid,period,days,weeks,terms,classroomid,subjectid\"><card subjectid=\"23604\" lessonid=\"*2\" classroomid=\"177\" period=\"4\" weeks=\"1\" terms=\"1\" days=\"0001000\"/></cards></timetable>";
			String year = "2012-01-01T";
			System.IO.StreamWriter write = new System.IO.StreamWriter("asc rec test.xml");
			write.WriteLine(testXml);
			write.Flush();
			write.Close();

			// Act:
			_parser.Parse(Path.GetFullPath("asc rec test.xml"), year);

			// Assert:
			Assert.AreEqual(1, _mockDbHandler.Bookings.Count, "The amoung of booking objects with recurrence is not correct!");
		}
	}
}
