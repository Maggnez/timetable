﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml;
using System.IO;
using Adaptor;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Adaptor.Celcat;
namespace AdaptorTest
{
	[TestClass]
	public class CelcatInputParserTests
	{
		InputParser parser = new CelcatInputParser();

		/// <summary>
		/// Tests the RoomParser method in the CelcatInputParser.
		/// </summary>
		[TestMethod]
		public void CelcatInputParserRoomTest()
		{
			// Arrange:
			string folder = Directory.GetCurrentDirectory(); ;
			var xmlToObject = new XmlToObject();
			String XmlRooms = "<ArrayOfRoomDTO xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/API.Models.Rooms'><RoomDTO><Description>sample string 4</Description><Equipment>sample string 5</Equipment><ID>1</ID><IsActive>true</IsActive><Name>sample string 3</Name><SeatCount>1</SeatCount></RoomDTO></ArrayOfRoomDTO>";
			String roomExpectedOutput = "CT_ROOM_room_id,unique_name,_name,_default_capacity1,1,sample string 3,1";
			var roomList = xmlToObject.RoomList(XmlRooms);
			parser.RoomParser(roomList, folder);
			string xmlRoomsOutput = File.ReadAllText("CelcatRoomsOutput.csv");

			// Act:
			string roomExpectedOutputNoSpaces = Regex.Replace(roomExpectedOutput, @"\s+", String.Empty);
			string roomActualOutputNoSpaces = Regex.Replace(xmlRoomsOutput, @"\s+", String.Empty);

			// Assert:
			Assert.AreEqual(roomExpectedOutputNoSpaces, roomActualOutputNoSpaces);
		}

		/// <summary>
		/// Tests the TeacherParser method in the CelcatInputParser.
		/// </summary>
		[TestMethod]
		public void CelcatInputParserTeacherTest()
		{
			// Arrange:
			string folder = Directory.GetCurrentDirectory(); ;
			var xmlToObject = new XmlToObject();
			String XmlTeachers = "<ArrayOfPersonDTO xmlns:i='http://www.w3.org/2001/XMLSchema-instance\' xmlns='http://schemas.datacontract.org/2004/07/API.Models.General.DTO\'><PersonDTO><Address>sample string 8</Address><Email>sample string 4</Email><Gender>sample string 10</Gender><HomePhone>sample string 7</HomePhone><ID>1</ID><MobilePhone>sample string 6</MobilePhone><Name>sample string 2</Name><PersonalEmail>sample string 5</PersonalEmail><Postal>sample string 9</Postal><SSN>sample string 3</SSN></PersonDTO></ArrayOfPersonDTO>";
			String teacherExpectedOutput = "CT_STAFF_unique_name,_staff_id,_namesamplestring3,samplestring3,samplestring2";
			var teacherList = xmlToObject.TeacherList(XmlTeachers);
			parser.TeacherParser(teacherList, folder);
			string xmlTeacherOutput = File.ReadAllText("CelcatTeacherOutput.csv");

			// Act:
			string teacherExpectedOutputNoSpaces = Regex.Replace(teacherExpectedOutput, @"\s+", String.Empty);
			string teacherActualOutputNoSpaces = Regex.Replace(xmlTeacherOutput, @"\s+", String.Empty);

			// Assert:
			Assert.AreEqual(teacherExpectedOutputNoSpaces, teacherActualOutputNoSpaces);
		}

		/// <summary>
		/// Tests the SubjectParser method in the CelcatInputParser.
		/// </summary>
		[TestMethod]
		public void CelcatInputParserCourseTest()
		{
			// Arrange:
			string folder = Directory.GetCurrentDirectory(); ;
			var xmlToObject = new XmlToObject();
			String XmlCourses = "<ArrayOfCourseInstanceDTO xmlns:i='http://www.w3.org/2001/XMLSchema-instance' xmlns='http://schemas.datacontract.org/2004/07/API.Models.Courses'><CourseInstanceDTO><CourseID>sample string 2</CourseID><DateBegin>2014-10-06T13:35:27.9780449+00:00</DateBegin><DateEnd>2014-10-06T13:35:27.9780449+00:00</DateEnd><DepartmentID>7</DepartmentID><ECTS>6.1</ECTS><ID>1</ID><MaxStudents>1</MaxStudents><Name>sample string 3</Name><NameEnglish>sample string 4</NameEnglish><RegisteredStudents>1</RegisteredStudents><Semester>sample string 5</Semester><Teachers><TeacherInCourse><CellPhone>samplestring4</CellPhone><Email>samplestring3</Email><FullName>samplestring2</FullName><HomePhone>samplestring5</HomePhone><SSN>samplestring1</SSN><TeacherType>samplestring7</TeacherType><TeacherTypeEN>samplestring8</TeacherTypeEN><TypeID>6</TypeID></TeacherInCourse><TeacherInCourse><CellPhone>samplestring4</CellPhone><Email>samplestring3</Email><TeacherType>samplestring7</TeacherType><FullName>samplestring2</FullName><HomePhone>samplestring5</HomePhone><SSN>samplestring1</SSN><TeacherTypeEN>samplestring8</TeacherTypeEN><TypeID>6</TypeID></TeacherInCourse></Teachers></CourseInstanceDTO></ArrayOfCourseInstanceDTO>";
			String subjectExpectedOutput = "CT_MODULE_module_id,_unique_name,_name1,1,samplestring3";
			var courseList = xmlToObject.CourseList(XmlCourses);
			parser.CourseParser(courseList, folder);
			string xmlSubjectOutput = File.ReadAllText("CelcatCourseOutput.csv");

			// Act:
			string subjectExpectedOutputNoSpaces = Regex.Replace(subjectExpectedOutput, @"\s+", String.Empty);
			string subjectActualOutputNoSpaces = Regex.Replace(xmlSubjectOutput, @"\s+", String.Empty);

			// Assert:
			Assert.AreEqual(subjectExpectedOutputNoSpaces, subjectActualOutputNoSpaces);
		}
	}
}
