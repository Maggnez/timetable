# CESAR (CEntris Schedule Automation bRidge) #

CESAR is a program that fetches information about courses, rooms and teachers in the University of Reykjavík and parses the data to xml or csv format files. These files can than be uploaded to a third party timetable schedule software where timetables are created. When the timetables have been created they are exported from the software as xml or csv and CESAR is run again. CESAR reads in these timetable files, parses them and requests a room booking to the schools database.

Here is a [wiki link](https://drive.google.com/file/d/0B0N86pBR4IlNeW9lSFBLV25jcEE/view?usp=sharing) in Icelandic that will help you get started on building the project and developing.

Here is a [wiki link](https://drive.google.com/file/d/0B0N86pBR4IlNVXNwMUtaRjQ2cnM/view?usp=sharing) in Icelandic that will help you set up the software and required programs to get started.

### More information ###

* Margrét S. Kristjánsdóttir - s. 849-9606 og Einar Þór Traustason - s. 868-9117